Es una aplicacion web creada en "ASP.NET core" con un patron de diseño MVC, en Visual Studio 2017.

Ambientacion del Proyecto:
- Se necesita correr las migraciones de BD con "Nuget Package Manager Console"
  - Para esto solo se abre el proyecto y nos dirigimos al Menu > Tools > Nuget Package Manager > Package Manager Console.
  - En la consola teclamos el siguiente comando "Update-Database -Verbose"
  - Ya ejecutadas las migraciones solo hay que ejecutar el proyecto.