﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.Models
{
    public class Sales
    {
        public int? SalesId { get; set; }
        public double Downpayment { get; set; }
        public double BonificationDownpayment { get; set; }
        public double Total { get; set; }
        public bool Inactive { get; set; }
    }
}
