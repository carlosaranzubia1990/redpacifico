﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.Models
{
    public class Product
    {
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string Model { get; set; }
        public double Price { get; set; }
        public int Stock { get; set; }
        public bool Inactive { get; set; }
    }
}
