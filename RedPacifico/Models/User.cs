﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.Models
{
    public class User: IdentityUser
    {
        public string NameUser { get; set; }
        public string LastName { get; set; }
        public bool Inactive { get; set; }
    }
}
