﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.Models
{
    public class Settings
    {
        public int? SettingsId { get; set; }
        public double FinancingRate { get; set; }
        public double Downpayment { get; set; }
        public int TimeLimit { get; set; }
        public bool Inactive { get; set; }
    }
}
