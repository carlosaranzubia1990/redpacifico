﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.Models
{
    public class SalesDetails
    {
        public int? SalesDetailsId { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double Import { get; set; }
        public int SalesId { get; set; }
        public virtual Sales Sales { get; set; }
        public bool Inactive { get; set; }
        public bool Available { get; set; }
    }
}
