﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.Models
{
    public class Customer
    {
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string LastName { get; set; }
        public string MothersLastName { get; set; }
        public string RFC { get; set; }
        public bool Inactive { get; set; }
    }
}
