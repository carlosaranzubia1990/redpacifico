﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;

namespace RedPacifico.Utilities
{

    public class MappingService
    {
        public T Map<F, T>(F from)
        {
            var json = JsonSerializer.Serialize(from);
            var to = JsonSerializer.Deserialize<T>(json);
            return to;
        }
    }
}
