﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RedPacifico.Models;

namespace RedPacifico.Models
{
    //public class RedPacificoContext : DbContext
    public class RedPacificoContext : IdentityDbContext<User>
    {
        public RedPacificoContext (DbContextOptions<RedPacificoContext> options)
            : base(options)
        {
        }

        public DbSet<RedPacifico.Models.Customer> Customer { get; set; }

        public DbSet<RedPacifico.Models.Product> Product { get; set; }

        public DbSet<RedPacifico.Models.Sales> Sales { get; set; }

        public DbSet<RedPacifico.Models.SalesDetails> SalesDetails { get; set; }

        public DbSet<RedPacifico.Models.Settings> Settings { get; set; }
    }
}
