﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RedPacifico.Migrations
{
    public partial class AddColumAvalaible : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SalesDetails_Sales_SalesId",
                table: "SalesDetails");

            migrationBuilder.AlterColumn<int>(
                name: "SalesId",
                table: "SalesDetails",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "Available",
                table: "SalesDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<double>(
                name: "Total",
                table: "Sales",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<bool>(
                name: "Inactive",
                table: "Sales",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<double>(
                name: "Downpayment",
                table: "Sales",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "BonificationDownpayment",
                table: "Sales",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AddForeignKey(
                name: "FK_SalesDetails_Sales_SalesId",
                table: "SalesDetails",
                column: "SalesId",
                principalTable: "Sales",
                principalColumn: "SalesId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SalesDetails_Sales_SalesId",
                table: "SalesDetails");

            migrationBuilder.DropColumn(
                name: "Available",
                table: "SalesDetails");

            migrationBuilder.AlterColumn<int>(
                name: "SalesId",
                table: "SalesDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Total",
                table: "Sales",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Inactive",
                table: "Sales",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Downpayment",
                table: "Sales",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "BonificationDownpayment",
                table: "Sales",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SalesDetails_Sales_SalesId",
                table: "SalesDetails",
                column: "SalesId",
                principalTable: "Sales",
                principalColumn: "SalesId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
