﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RedPacifico.Models;
using RedPacifico.ViewModels;
using RedPacifico.Utilities;

namespace RedPacifico.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        private readonly RedPacificoContext _context;
        private MappingService map;

        public CustomerController(RedPacificoContext context)
        {
            _context = context;
            map = new MappingService();
        }

        // GET: Customer
        public async Task<IActionResult> Index()
        {
            var settings = await _context.Settings.FirstOrDefaultAsync();
            if (settings == null)
                return RedirectToAction("Edit", "Settings");

            var listCustomers = await _context.Customer.ToListAsync();
            var vm = map.Map<IEnumerable<Customer>, IEnumerable<CustomerViewModels>>(listCustomers);
            return View(vm);
        }

        // GET: Customer/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var customer = await _context.Customer
                .FirstOrDefaultAsync(m => m.CustomerId == id);

            if (customer == null)
                return NotFound();

            var vm = map.Map<Customer, CustomerViewModels>(customer);
            return View(vm);
        }

        // GET: Customer/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CustomerId,CustomerName,LastName,MothersLastName,RFC")] CustomerViewModels customer)
        {
            if (ModelState.IsValid)
            {
                var vm = map.Map<CustomerViewModels, Customer>(customer);
                _context.Add(vm);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // GET: Customer/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var customer = await _context.Customer.FindAsync(id);
            if (customer == null)
                return NotFound();

            var vm = map.Map<Customer, CustomerViewModels>(customer);
            return View(vm);
        }

        // POST: Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, [Bind("CustomerId,CustomerName,LastName,MothersLastName,RFC")] CustomerViewModels customer)
        {
            if (id != customer.CustomerId)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var vm = map.Map<CustomerViewModels, Customer>(customer);
                    _context.Update(vm);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(customer.CustomerId))
                        return NotFound();
                    else
                        throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // GET: Customer/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var customer = await _context.Customer
                .FirstOrDefaultAsync(m => m.CustomerId == id);

            if (customer == null)
                return NotFound();

            var vm = map.Map<Customer, CustomerViewModels>(customer);
            return View(vm);
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var customer = await _context.Customer.FindAsync(id);
            _context.Customer.Remove(customer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CustomerExists(int? id)
        {
            return _context.Customer.Any(e => e.CustomerId == id);
        }
    }
}