﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RedPacifico.Models;
using RedPacifico.ViewModels;
using RedPacifico.Utilities;

namespace RedPacifico.Controllers
{
    [Authorize]
    public class SalesDetailsController : Controller
    {
        private readonly RedPacificoContext _context;
        private MappingService map;

        public SalesDetailsController(RedPacificoContext context)
        {
            _context = context;
            map = new MappingService();
        }

        private List<SelectListItem> getCustomers()
        {
            return _context.Customer.OrderBy(u => u.CustomerName)
                .Select(u => new SelectListItem
                {
                    Value = u.CustomerId.ToString(),
                    Text = u.CustomerName
                }).ToList();
        }

        private List<SelectListItem> getProducts()
        {
            return _context.Product.OrderBy(u => u.ProductName)
                .Select(u => new SelectListItem
                {
                    Value = u.ProductId.ToString(),
                    Text = u.ProductName
                }).ToList();
        }

        // GET: SalesDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var salesDetails = await _context.SalesDetails.FindAsync(id);
            if (salesDetails == null)
                return NotFound();

            var vm = map.Map<SalesDetails, SalesDetailsViewModels>(salesDetails);
            vm.Product = getProducts();
            vm.Customer = getCustomers();
            return View(vm);
        }

        // POST: SalesDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, [Bind("SalesDetailsId,ProductId,Quantity,Price,Import,SalesId,CustomerId")] SalesDetailsViewModels vm)
        {
            if (id != vm.SalesDetailsId)
                return NotFound();

            var settings = _context.Settings.First(x => !x.Inactive);

            if (ModelState.IsValid)
            {
                try
                {
                    var product = _context.Product.First(x => x.ProductId == vm.ProductId);
                    var reserved = _context.SalesDetails.Where(x => x.SalesId == vm.SalesId && !x.Inactive).Sum(x => x.Quantity);

                    if (product.Stock < (reserved + vm.Quantity))
                    {
                        vm.MessageError = "“El producto seleccionado no cuenta con existencia, favor de verificar”";
                    }
                    else
                    {
                        var price = product.Price * (1 + (settings.FinancingRate * settings.TimeLimit) / 100);
                        vm.Price = price;
                        vm.Import = price * vm.Quantity;
                        var salesDetails = map.Map<SalesDetailsViewModels, SalesDetails>(vm);
                        _context.Update(salesDetails);
                        await _context.SaveChangesAsync();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SalesDetailsExists(vm.SalesDetailsId))
                        return NotFound();
                    else
                        throw;
                }
            }
            vm.Product = getProducts();
            vm.Customer = getCustomers();
            return View(vm);
        }

        private bool SalesDetailsExists(int? id)
        {
            return _context.SalesDetails.Any(e => e.SalesDetailsId == id);
        }
    }
}