﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RedPacifico.Models;
using RedPacifico.ViewModels;
using RedPacifico.Utilities;

namespace RedPacifico.Controllers
{
    [Authorize]
    public class SalesController : Controller
    {
        private readonly RedPacificoContext _context;
        private MappingService map;

        public SalesController(RedPacificoContext context)
        {
            _context = context;
            map = new MappingService();
        }

        // GET: Sales
        public async Task<IActionResult> Index()
        {
            var settings = await _context.Settings.FirstOrDefaultAsync();
            if (settings == null)
                return RedirectToAction("Edit", "Settings");

            return View(getListSales());
        }

        public IActionResult Edit(int? id)
        {
            var sale = _context.Sales.First(x => x.SalesId == id);
            var vm = map.Map<Sales, SalesViewModels>(sale);
            vm.Details = getSalesDetails(id);
            vm.Customers = getCustomers();
            vm.Products = getProducts();
            calculate(ref vm);
            return View("Create", vm);
        }

        public void calculate(ref SalesViewModels vm)
        {
            var settings = _context.Settings.First(x => !x.Inactive);
            var totalImport = vm.Details.Sum(x => x.Import);
            vm.Downpayment = (settings.Downpayment / 100) * totalImport;
            vm.BonificationDownpayment = (vm.Downpayment * ((settings.FinancingRate * settings.TimeLimit) / 100));
            vm.Total = totalImport - vm.Downpayment - vm.BonificationDownpayment;
        }

        // GET: Sales/Create
        public IActionResult Create()
        {
            SalesViewModels vm = new SalesViewModels();
            vm.Products = getProducts();
            vm.Customers = getCustomers();
            vm.Details = new List<SalesDetails>();
            return View(vm);
        }

        // POST: Sales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SalesId,CustomerId,ProductId,Quantity")] SalesViewModels vm)
        {
            if (ModelState.IsValid)
            {
                if (vm.SalesId == null)
                {
                    vm.Downpayment = 0;
                    vm.BonificationDownpayment = 0;
                    vm.Total = 0;
                    var sales = map.Map<SalesViewModels, Sales>(vm);
                    sales.Inactive = true;
                    _context.Add(sales);
                    await _context.SaveChangesAsync();
                    vm.SalesId = sales.SalesId;
                }

                var product = _context.Product.First(x => x.ProductId == vm.ProductId);
                var reserved = _context.SalesDetails.Where(x => x.SalesId == vm.SalesId && !x.Inactive).Sum(x => x.Quantity);
                if (product.Stock < (reserved + vm.Quantity))
                {
                    vm.MessageError = "“El producto seleccionado no cuenta con existencia, favor de verificar”";
                }
                else
                {
                    var settings = _context.Settings.First(x => !x.Inactive);
                    var price = product.Price * (1 + (settings.FinancingRate * settings.TimeLimit) / 100);
                    var import = price * vm.Quantity;
                    var salesDetails = new SalesDetails
                    {
                        CustomerId = vm.CustomerId,
                        ProductId = vm.ProductId,
                        SalesId = vm.SalesId.Value,
                        Inactive = false,
                        Import = import.Value,
                        Price = price,
                        Quantity = vm.Quantity.Value,
                        Available = false
                    };
                    _context.Add(salesDetails);
                    await _context.SaveChangesAsync();
                }
                vm.Details = getSalesDetails(vm.SalesId);
            }
            vm.Customers = getCustomers();
            vm.Products = getProducts();
            calculate(ref vm);
            return View(vm);
        }

        public async Task<IActionResult> Save(int? id)
        {
            var settings = _context.Settings.First(x => !x.Inactive);
            var sale = _context.Sales.First(x => x.SalesId == id);
            var vm = map.Map<Sales, SalesViewModels>(sale);
            vm.Details = getSalesDetails(id);
            calculate(ref vm);
            sale.BonificationDownpayment = vm.BonificationDownpayment;
            sale.Downpayment = vm.Downpayment;
            sale.Total = vm.Total;
            sale.Inactive = false;
            _context.Sales.Update(sale);

            var itemsToUpdate = await _context.SalesDetails.Where(x => x.SalesId == id && !x.Inactive).ToListAsync();
            foreach (var item in itemsToUpdate)
                item.Available = true;

            await _context.SaveChangesAsync();
            return View("Index", getListSales());
        }

        public List<SalesViewModels> getListSales()
        {
            var listSales = _context.Sales.Where(x => !x.Inactive).ToList();
            var customer = new Customer();
            var vm = map.Map<List<Sales>, List<SalesViewModels>>(listSales);
            foreach (var item in vm)
            {
                item.Details = _context.SalesDetails
                        .Include(s => s.Customer)
                        .Include(s => s.Product)
                        .Where(x => x.SalesId == item.SalesId && !x.Inactive && x.Available)
                        .ToList();
            }
            return vm;
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var saleDetail = _context.SalesDetails.First(x => x.SalesDetailsId == id);
            var sale = _context.Sales.First(x => x.SalesId == saleDetail.SalesId);
            saleDetail.Inactive = true;
            _context.SalesDetails.Update(saleDetail);
            await _context.SaveChangesAsync();
            var vm = map.Map<Sales, SalesViewModels>(sale);
            vm.Customers = getCustomers();
            vm.Products = getProducts();
            vm.Details = getSalesDetails(sale.SalesId);
            calculate(ref vm);
            return View("Create", vm);
        }

        private async Task<SalesViewModels> getSales(int? id)
        {
            var sales = await _context.Sales.FindAsync(id);
            var vm = map.Map<Sales, SalesViewModels>(sales);
            vm.Customers = getCustomers();
            vm.Products = getProducts();
            vm.Details = getSalesDetails(sales.SalesId);
            return vm;
        }

        private List<SelectListItem> getCustomers()
        {
            return _context.Customer.OrderBy(u => u.CustomerName)
                .Select(u => new SelectListItem
                {
                    Value = u.CustomerId.ToString(),
                    Text = u.CustomerName
                }).ToList();
        }

        private List<SelectListItem> getProducts()
        {
            return _context.Product.OrderBy(u => u.ProductName)
                .Select(u => new SelectListItem
                {
                    Value = u.ProductId.ToString(),
                    Text = u.ProductName
                }).ToList();
        }

        private List<SalesDetails> getSalesDetails(int? id)
        {
            return _context.SalesDetails
                    .Include(u => u.Product)
                    .Include(u => u.Customer)
                    .Where(u => u.SalesId == id && u.Inactive == false).ToList();
        }
    }
}