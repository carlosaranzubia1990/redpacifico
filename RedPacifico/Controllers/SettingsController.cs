﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RedPacifico.Models;
using RedPacifico.ViewModels;
using RedPacifico.Utilities;

namespace RedPacifico.Controllers
{
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly RedPacificoContext _context;
        private MappingService map;

        public SettingsController(RedPacificoContext context)
        {
            _context = context;
            map = new MappingService();
        }

        // GET: Settings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var settings = await _context.Settings.FirstOrDefaultAsync();
            if (settings == null)
                settings = new Settings();

            var vm = map.Map<Settings, SettingsViewModels>(settings);
            return View(vm);
        }

        // POST: Settings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, [Bind("SettingsId,FinancingRate,Downpayment,TimeLimit")] SettingsViewModels settings)
        {
            if (ModelState.IsValid || settings.SettingsId == null)
            {
                try
                {
                    var vm = map.Map<SettingsViewModels, Settings>(settings);
                    if (settings.SettingsId == null)
                        _context.Add(vm);
                    else
                        _context.Update(vm);
                    await _context.SaveChangesAsync();

                    settings = map.Map<Settings, SettingsViewModels>(vm);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SettingsExists(settings.SettingsId))
                        return NotFound();
                    else
                        throw;
                }
                return RedirectToAction("Index", "Sales");
            }
            return View(settings);
        }

        private bool SettingsExists(int? id)
        {
            return _context.Settings.Any(e => e.SettingsId == id);
        }
    }
}