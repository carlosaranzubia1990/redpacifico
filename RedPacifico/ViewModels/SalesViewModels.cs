﻿using Microsoft.AspNetCore.Mvc.Rendering;
using RedPacifico.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.ViewModels
{
    public class SalesViewModels
    {
        [Display(Name = "Venta")]
        public int? SalesId { get; set; }

        [Display(Name = "Cliente")]
        [Required(ErrorMessage = "El cliente es requerido")]
        public int CustomerId { get; set; }
        public List<SelectListItem> Customers { get; set; }

        [Display(Name = "Producto")]
        [Required(ErrorMessage = "El producto es requerido")]
        public int ProductId { get; set; }
        public List<SelectListItem> Products { get; set; }

        [Display(Name = "Cantidad")]
        [Required(ErrorMessage = "La cantidad es requerido")]
        [RegularExpression(@"^[0-9]+$",
            ErrorMessage = "Solo se permiten Números")]
        public int? Quantity { get; set; }

        [Display(Name = "Enganche")]
        [RegularExpression(@"^[0-9.]+$",
            ErrorMessage = "Solo se permiten Números")]
        public double Downpayment { get; set; }

        [Display(Name = "Bonificacion Enganche")]
        [RegularExpression(@"^[0-9.]+$",
            ErrorMessage = "Solo se permiten Números")]
        public double BonificationDownpayment { get; set; }

        [Display(Name = "Total")]
        public double Total { get; set; }
        public List<SalesDetails> Details { get; set; }
        public string MessageError { get; set; }
    }
}
