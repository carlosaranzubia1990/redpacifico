﻿using Microsoft.AspNetCore.Mvc.Rendering;
using RedPacifico.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.ViewModels
{
    public class SalesDetailsViewModels
    {
        public int SalesDetailsId { get; set; }
        [Display(Name = "Nombre Cliente")]
        public int? CustomerId { get; set; }
        public List<SelectListItem> Customer { get; set; }
        [Display(Name = "Producto")]
        public int? ProductId { get; set; }
        public List<SelectListItem> Product { get; set; }
        [Display(Name = "Cantidad")]
        public int Quantity { get; set; }
        [Display(Name = "Presio")]
        public double Price { get; set; }
        [Display(Name = "Importe")]
        public double Import { get; set; }
        public int? SalesId { get; set; }
        public List<SelectListItem> Sales { get; set; }
        public string MessageError { get; set; }
    }
}
