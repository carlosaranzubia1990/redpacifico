﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.ViewModels
{
    public class CustomerViewModels
    {
        [Display(Name = "Cliente")]
        public int? CustomerId { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "“Favor de ingresar Nombre es obligatorio”")]
        [MaxLength(20, ErrorMessage = "El maximo de caracteres es 20")]
        [RegularExpression(@"^[A-Z a-z0-9ÑñáéíóúÁÉÍÓÚ\\-\\_]+$",
            ErrorMessage = "El Nombre debe ser alfanumérico.")]
        public string CustomerName { get; set; }

        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "“Favor de ingresar Apellido Paterno es obligatorio”")]
        [MaxLength(20, ErrorMessage = "El maximo de caracteres es 20")]
        [RegularExpression(@"^[A-Z a-z0-9ÑñáéíóúÁÉÍÓÚ\\-\\_]+$",
            ErrorMessage = "El Apellido Paterno debe ser alfanumérico.")]
        public string LastName { get; set; }

        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "“Favor de ingresar Apellido Materno es obligatorio”")]
        [MaxLength(20, ErrorMessage = "El maximo de caracteres es 20")]
        [RegularExpression(@"^[A-Z a-z0-9ÑñáéíóúÁÉÍÓÚ\\-\\_]+$",
            ErrorMessage = "El Apellido Materno debe ser alfanumérico.")]
        public string MothersLastName { get; set; }

        [Display(Name = "RFC")]
        [Required(ErrorMessage = "“Favor de ingresar RFC es obligatorio”")]
        [MaxLength(20, ErrorMessage = "El maximo de caracteres es 20")]
        [RegularExpression(@"^[A-Z a-z0-9ÑñáéíóúÁÉÍÓÚ\\-\\_]+$",
            ErrorMessage = "El RFC debe ser alfanumérico.")]
        public string RFC { get; set; }
    }
}
