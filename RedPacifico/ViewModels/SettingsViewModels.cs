﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.ViewModels
{
    public class SettingsViewModels
    {
        [Display(Name = "Configuracion")]
        public int? SettingsId { get; set; }

        [Display(Name = "Taza de Financiamiento")]
        [Required(ErrorMessage = "La taza de financiamiento es requerida")]
        [RegularExpression(@"^[0-9.]+$",
            ErrorMessage = "Solo se permiten Números")]
        public double? FinancingRate { get; set; }

        [Display(Name = "% Enganche")]
        [Required(ErrorMessage = "El % de enganche es requerido")]
        [RegularExpression(@"^[0-9.]+$",
            ErrorMessage = "Solo se permiten Números")]
        public double? Downpayment { get; set; }

        [Display(Name = "Plazo")]
        [Required(ErrorMessage = "El plazo es requerido")]
        [RegularExpression(@"^[0-9]+$",
            ErrorMessage = "Solo se permiten Números")]
        public int? TimeLimit { get; set; }
    }
}
