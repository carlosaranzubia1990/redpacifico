﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RedPacifico.ViewModels
{
    public class ProductViewModels
    {
        [Display(Name = "Producto")]
        public int? ProductId { get; set; }

        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "“Favor de ingresar Descripción es obligatorio”")]
        [MaxLength(40, ErrorMessage = "El maximo de caracteres es 40")]
        [RegularExpression(@"^[A-Z a-z0-9ÑñáéíóúÁÉÍÓÚ\\-\\_]+$",
            ErrorMessage = "La Descripción debe ser alfanumérico.")]
        public string ProductName { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "“Favor de ingresar Modelo es obligatorio”")]
        [MaxLength(20, ErrorMessage = "El maximo de caracteres es 20")]
        [RegularExpression(@"^[A-Z a-z0-9ÑñáéíóúÁÉÍÓÚ\\-\\_]+$",
            ErrorMessage = "El Modelo debe ser alfanumérico.")]
        public string Model { get; set; }

        [Display(Name = "Precio")]
        [Required(ErrorMessage = "“Favor de ingresar Precio es obligatorio”")]
        [RegularExpression(@"^[0-9.]+$",
            ErrorMessage = "Solo se permiten Números")]
        public double Price { get; set; }

        [Display(Name = "Existencia")]
        [Required(ErrorMessage = "“Favor de ingresar Existencia es obligatorio”")]
        public int Stock { get; set; }
    }
}
